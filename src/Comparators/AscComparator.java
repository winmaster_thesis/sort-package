package Comparators;

import java.util.Comparator;

// Comparator for ascending order

public class AscComparator<T extends Comparable<T>> implements Comparator<T>
{
    @Override
    public int compare(T objectFirst, T objectSecond)
    {
       if(objectFirst instanceof String && objectSecond instanceof  String)
       {
           return ((String) objectFirst).compareTo((String) objectSecond);
       }
       else
       if(objectFirst instanceof Integer && objectSecond instanceof Integer)
       {
            return  (Integer)objectFirst < (Integer)objectSecond ? -1 : ((Integer) objectFirst).equals((Integer) objectSecond) ? 0 : 1;
       }
      return -1;
    }
}
