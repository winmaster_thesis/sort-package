package Factory;

import Helpers.CompareMethod;
import Helpers.SortingAlgorithms;
import Sorting.*;

import java.util.ArrayList;

public class Factory<T extends Comparable<T>>
{
    private ArrayList<T> sortArray;
    private CompareMethod compareMethod;

    public Factory(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        sortArray = _sortArray;
        compareMethod = _compareMethod;
    }

    public BaseSort<T> produceSorter(String algorithmName)
    {
        switch (algorithmName)
        {
            case SortingAlgorithms.BUBBLE_SORT:
            {
                return new BubbleSort<>(sortArray, compareMethod);
            }
            case SortingAlgorithms.INSERT_SORT:
            {
                return new InsertSort<>(sortArray, compareMethod);
            }
            case SortingAlgorithms.SELECT_SORT:
            {
                return new SelectSort<>(sortArray, compareMethod);
            }
            case SortingAlgorithms.BOGO_SORT:
            {
                return new BogoSort<>(sortArray, compareMethod);
            }
            case SortingAlgorithms.GNOME_SORT:
            {
                return new GnomeSort<>(sortArray, compareMethod);
            }
            default:
            {
                return null;
            }
        }
    }
}
