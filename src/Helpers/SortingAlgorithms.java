package Helpers;

public interface SortingAlgorithms
{
    String BUBBLE_SORT = "Bubble Sort";
    String INSERT_SORT = "Insert Sort";
    String SELECT_SORT = "Select Sort";
    String BOGO_SORT = "Bogo Sort";
    String GNOME_SORT = "Gnome Sort";
}
