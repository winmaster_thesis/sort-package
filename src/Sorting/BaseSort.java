package Sorting;

import Comparators.AscComparator;
import Comparators.DescComparator;
import Helpers.CompareMethod;
import java.util.ArrayList;
import java.util.Comparator;

public abstract class BaseSort<T extends Comparable<T>> implements IBaseSort
{
    protected String algorithmName;
    ArrayList<T> sortArray;
    Comparator<T> sortComparator;

    BaseSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        sortArray = new ArrayList<>();
        sortArray.addAll(_sortArray);
        sortComparator = _compareMethod.equals(CompareMethod.DESCENDING) ? new DescComparator<>() : new AscComparator<>();
    }

    public void setSortArray(ArrayList<T> newSortArray)
    {
        sortArray=newSortArray;
    }

    public ArrayList<T> getSortArray()
    {
        return sortArray;
    }

    public void executeSorting()
    {
        while(!isSorted())
        sortStep();
    }
}
