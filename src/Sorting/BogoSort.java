package Sorting;

import Helpers.CompareMethod;
import Helpers.SortingAlgorithms;

import java.util.ArrayList;
import java.util.Collections;

public class BogoSort<T extends Comparable<T>> extends BaseSort
{
    private int shuffleCounter;

    public BogoSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        super(_sortArray, _compareMethod);
        shuffleCounter=0;
        algorithmName = SortingAlgorithms.BOGO_SORT;
    }

    public boolean isSorted()
    {
        boolean isOver=true;

        for(int i=0; i<sortArray.size()-1; i++)
        {
            if(sortComparator.compare(sortArray.get(i),sortArray.get(i+1))>0)
            {
                isOver=false;
                break;
            }
        }
        return isOver;
    }


    /*
        Every sortStep:
        - shufle the sortArray
     */

    public void sortStep()
    {
        Collections.shuffle(sortArray);
        shuffleCounter++;
    }

    public String printSortArray()
    {
        return  "Dane: "+ sortArray;
    }

    public int getShuffleCounter()
    {
        return  shuffleCounter;
    }
}
