package Sorting;

public interface IBaseSort
{
    boolean isSorted();
    void sortStep();
    String printSortArray();
}
