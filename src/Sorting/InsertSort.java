package Sorting;

import Helpers.CompareMethod;
import Helpers.SortingAlgorithms;

import java.util.ArrayList;
import java.util.Collections;

public class InsertSort<T extends Comparable<T>> extends BaseSort
{
    private ArrayList<T> destinationArray;

    public InsertSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        super(_sortArray, _compareMethod);
        algorithmName = SortingAlgorithms.INSERT_SORT;
        destinationArray= new ArrayList<>();
    }

    public boolean isSorted()
    {
        return sortArray.isEmpty();
    }

    /*
        Every sortStep:
        -add first element of sortArray to destinationArray, then delete it
        -put all elements in destinationArray in order
     */
    public void sortStep()
    {
        destinationArray.add((T) sortArray.get(0));
        sortArray.remove(0);

        Collections.sort(destinationArray,sortComparator);
    }

    public String printSortArray()
    {
        return "Dane: "+ destinationArray+ "|"+sortArray;
    }
}

