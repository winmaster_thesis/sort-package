package Sorting;

import Helpers.CompareMethod;
import Helpers.SortingAlgorithms;

import java.util.ArrayList;

public class SelectSort<T extends Comparable<T>> extends BaseSort
{
    private ArrayList<T> destinationArray;

    public SelectSort(ArrayList<T> _sortArray, CompareMethod _compareMethod)
    {
        super(_sortArray, _compareMethod);
        algorithmName = SortingAlgorithms.SELECT_SORT;
        destinationArray=new ArrayList<>();
    }

    public boolean isSorted()
    {
        return sortArray.isEmpty();
    }

    /*
        Every sortStep:
        -find maximal or minimal value in set (depends on  compareMethod)
        -add this element to destinationArray
        -put first element from sortArray on min/max index
        -delete first element from sortArray
     */
    public void sortStep()
    {
        Object elem=sortArray.get(0);
        int elemIndex=0;

        for(Object t: sortArray)
        {
            if(sortComparator.compare(t,elem)<=0)
            {
                elem=t;
                elemIndex=sortArray.indexOf(t);
            }
        }

        destinationArray.add((T) elem);
        sortArray.set(elemIndex,sortArray.get(0));
        sortArray.remove(0);
    }

    public String printSortArray()
    {
        return "Dane: "+destinationArray+ "|"+sortArray;
    }
}
